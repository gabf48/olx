package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utils.BaseClass;

public class ProductPage extends BaseClass {


    private WebDriver driver;

    public ProductPage(WebDriver driver) {
        this.driver = driver;
    }
    public void clickFavouriteButton() {
        driver.findElement(By.cssSelector("#offerdescription [data-icon=\"star\"]")).click();
    }
}
