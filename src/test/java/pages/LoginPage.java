package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utils.BaseClass;

public class LoginPage extends BaseClass {

    private WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    private void openLoginPage() {
        driver.manage().window().maximize();
        driver.get("https://www.olx.ro/cont/?ref%5B0%5D%5Baction%5D=myaccount&ref%5B0%5D%5Bmethod%5D=index");
    }

    private void typeEmail(String email){
        driver.findElement(By.id("userEmail")).sendKeys(email);
    }

    private void typePassword(String password) {
        driver.findElement(By.id("userPass")).sendKeys(password);
    }

    private void clickLoginButton() {
        driver.findElement(By.id("se_userLogin")).click();
    }

    private void verifyIfTheElementIsDisplayed() {
        Assert.assertTrue(driver.findElement(By.id("topLoginLink")).isDisplayed());
    }

    public void loginOlx(String email, String password){
        openLoginPage();
        typeEmail(email);
        typePassword(password);
        clickLoginButton();
        verifyIfTheElementIsDisplayed();
    }
}
