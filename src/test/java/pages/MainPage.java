package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import utils.BaseClass;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class MainPage extends BaseClass {

    private WebDriver driver;

    public MainPage(WebDriver driver) {
        this.driver = driver;
    }

    public void clickOnMainPage() {
        driver.findElement(By.id("headerLogo")).click();
    }

    public void selectRandomProductJustWithImage() {
        List<WebElement> categoryList = driver.findElements(By.cssSelector("[class=\"gallerywide clr normal\"] li div a img"));
        int size = categoryList.size();
        int randNumber = ThreadLocalRandom.current().nextInt(0, size);
        categoryList.get(randNumber).click();
    }
}
