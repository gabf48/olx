package tests;



import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import pages.LoginPage;
import utils.BaseClass;

import static utils.Constants.*;


public class LoginTests extends BaseClass {

    private LoginPage loginPage;
    WebDriver driver;

    @After
    public void tearDown() {
        driver.quit();
    }

    @Before()
    public void setUp() {
        driver = initializeDriver();
        loginPage = new LoginPage(driver);
    }

    @Test
    public void loginWithValidCredentials() {
    loginPage.loginOlx(VALID_EMAIL, VALID_PASSWORD);
    }
}
