package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BaseClass {

    private WebDriver driver;

    public WebDriver initializeDriver() {
        return driver = new ChromeDriver();
    }
}
