package tests;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import pages.LoginPage;
import pages.MainPage;
import pages.ProductPage;
import utils.BaseClass;

import static utils.Constants.*;

public class FavouriteTests extends BaseClass {

    WebDriver driver;
    ProductPage productPage;

    @Before()
    public void setUp() {
        driver = initializeDriver();
        loginPage = new LoginPage(driver);
        mainPage = new MainPage(driver);
        productPage = new ProductPage(driver);
    }

    private LoginPage loginPage;
    private MainPage mainPage;


    @Test
    public void checkIfTheProductWasAddedAtFavourite() {
        loginPage.loginOlx(VALID_EMAIL, VALID_PASSWORD);
        mainPage.clickOnMainPage();
        mainPage.selectRandomProductJustWithImage();
        productPage.clickFavouriteButton();
    }
}
